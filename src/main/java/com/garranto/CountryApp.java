package com.garranto;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class CountryApp {

    public static void main(String[] args) {
        System.out.println("welcome to my country app");
        System.out.println("============================");


//        client object for sending request
        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

//        construct the request with the necesary url, headers and paramters

        Request request = new Request.Builder()
                .url("https://restcountries.com/v3.1/all")
                .method("GET", null)
                .build();

        try {
            Response response = client.newCall(request).execute();

//            json array represented in string
            String countries = response.body().string();
//            System.out.println(countries);

            JsonArray  countriesArray = gson.fromJson(countries,JsonArray.class);
            System.out.println(countriesArray.size());

            for(JsonElement countryElement: countriesArray){
                JsonObject countryObject = countryElement.getAsJsonObject();
                JsonObject nameObject = countryObject.getAsJsonObject("name");
                JsonPrimitive officialName = nameObject.getAsJsonPrimitive("official");
                System.out.println(officialName);
            }


        } catch (IOException e) {
            System.out.println("Failed to fetch countries from the country rest");
        }
    }
}


//1. ok http client library
//print all the country name


// convert into array
//convert each array items into a appropriate object type
//appply logic

//gson ----  parsing json in java

//jsonArray
//jsonobject
//jsonprimitive