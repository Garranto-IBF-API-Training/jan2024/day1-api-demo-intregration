package com.garranto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import okhttp3.*;

import java.io.IOException;
import java.util.Scanner;

public class CurrencyConversionApp {
    public static void main(String[] args) {
        System.out.println("Welcome to Currency Convert APP");
        System.out.println("==================================");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the amount to be converted");
        String amount = scanner.next();
        System.out.println("Enter the source Currency");
        String source = scanner.next();
        System.out.println("Enter the target currency");
        String target = scanner.next();

        OkHttpClient client = new OkHttpClient().newBuilder().build();

        Gson gson = new Gson();

        String apiKey = "GfRrzo6glVur4K5kJAsQxGWhmAKi1U4x";
//        String endpoint = "https://api.apilayer.com/exchangerates_data/convert?to="+to+"&from="+from+"&amount="+amount;
        String endpoint = "https://api.apilayer.com/exchangerates_data/convert";

        HttpUrl.Builder builder = HttpUrl.parse(endpoint).newBuilder();
        builder.addQueryParameter("amount",amount);
        builder.addQueryParameter("from",source);
        builder.addQueryParameter("to",target);

        HttpUrl url = builder.build();

//        construct the request with the necesary url, headers and paramters

        Request request = new Request.Builder()
                .url(url)
                .addHeader("apiKey",apiKey)
                .method("GET", null)
                .build();


        try {
            Response response = client.newCall(request).execute();

//            json object represented in string
            String result = response.body().string();
            JsonObject resultObject = gson.fromJson(result, JsonObject.class);
            JsonPrimitive convertedAmount = resultObject.getAsJsonPrimitive("result");
            System.out.println("The converted amount is: "+convertedAmount);
//            System.out.println(result);
        } catch (IOException e) {
            System.out.println("Failed to convert: Something went wrong");
        }

    }
}


//1. get the amount, from and to
//2. construct a request using uri, parameters and header
//send the request and process the reponse


//100, sgd and inr --- 64.890809